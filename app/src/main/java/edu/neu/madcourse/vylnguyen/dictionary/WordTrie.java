package edu.neu.madcourse.vylnguyen.dictionary;
// Courtesy of LeetCode: http://www.programcreek.com/2014/05/leetcode-implement-trie-prefix-tree-java/

class TrieNode implements java.io.Serializable{
    TrieNode[] arr;
    boolean isEnd;
    // Initialize your data structure here.
    public TrieNode() {
        this.arr = new TrieNode[26];
    }

}

public class WordTrie implements java.io.Serializable{
    private TrieNode root;
    private String key;

    public WordTrie() {
        root = new TrieNode();
    }

    public void setKey(String key){
        this.key = key;
    }

    public String getKey(){
        return key;
    }

    // Inserts a word into the trie.
    public void insert(String word) {
        TrieNode p = root;
        for(int i=0; i<word.length(); i++){
            char c = word.charAt(i);
            int index = c-'a';
            if(p.arr[index]==null){
                TrieNode temp = new TrieNode();
                p.arr[index]=temp;
                p = temp;
            }else{
                p=p.arr[index];
            }
        }
        p.isEnd=true;
    }

    // Returns if the word is in the trie.
    public boolean search(String word) {
        word.toLowerCase();
        TrieNode p = searchNode(word);
        if(p==null){
            return false;
        }else{
            if(p.isEnd)
                return true;
        }

        return false;
    }

    public TrieNode searchNode(String s){
        TrieNode p = root;
        for(int i=0; i<s.length(); i++){
            char c= s.charAt(i);
            int index = c-'a';
            try{
                if(p.arr[index]!=null){
                    p = p.arr[index];
                }else{
                    return null;
                }
            }
            catch(ArrayIndexOutOfBoundsException error){
                error.printStackTrace();
            }

        }

        if(p==root)
            return null;

        return p;
    }
}
