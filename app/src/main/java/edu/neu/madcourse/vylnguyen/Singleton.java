package edu.neu.madcourse.vylnguyen;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.HashMap;

import edu.neu.madcourse.vylnguyen.dictionary.Dictionary;
import edu.neu.madcourse.vylnguyen.dictionary.WordTrie;

import static edu.neu.madcourse.vylnguyen.R.raw.g;

//import static edu.neu.madcourse.vylnguyen.Singleton.initContext;

/**
 * Created by nguyenvyl on 2/5/17.
 */

public class Singleton {
    private static Singleton instance = null;
    public HashMap<String, WordTrie> dictionary;
    public ArrayList<String> nineLetterWords;
    public Dictionary dictionaryClass;
    private Context context;
    private DatabaseReference mDatabase;
    private String userKey;
    private String username;
    private static final String SERVER_KEY = "key=AAAALnc5Jwk:APA91bFC4Hd86H8TcVwdw7rJnbEbkoMiasg2GKUWq7s6Vz1n05ekdLHy9cdJbX0nqfZYOKWgGh5bST7FJYi5hvriaBAR0IMnXtfEv7gpuOatXgU1yvTqNusGGSHGz4t3pkhMRMZImZ0d";
    private long timeRemaining;
    private static final String TAG = "Singleton";

    private Singleton(){
        this.dictionary = new HashMap<>();
        this.context = MyApplication.getAppContext();
        this.dictionaryClass = new Dictionary(this.context);
    }

    public static Singleton getInstance(){
        if(instance == null){
            instance = new Singleton();
        }
        return instance;
    }

    public HashMap<String, WordTrie> getDictionary() {
        return dictionary;
    }

    public Dictionary getDictionaryClass() {
        return dictionaryClass;
    }


    public ArrayList<String> get9letters() {
        return this.nineLetterWords;
    }

    public void set9letters(ArrayList<String> nineLetterWords) {
        this.nineLetterWords = nineLetterWords;
    }

    public void setDictionary(HashMap<String, WordTrie> dictionary) {
        this.dictionary = dictionary;
    }

    public void addTrie(WordTrie trie){
        this.dictionary.put(trie.getKey(), trie);
    }

    public DatabaseReference getmDatabase() {
        return mDatabase;
    }

    public void setmDatabase(DatabaseReference mDatabase) {
        this.mDatabase = mDatabase;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public static String getServerKey() {
        return SERVER_KEY;
    }

    public long getTimeRemaining() {
        return timeRemaining;
    }

    public void setTimeRemaining(long timeRemaining) {
        this.timeRemaining = timeRemaining;
    }
}
