package edu.neu.madcourse.vylnguyen.dictionary;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Created by nguyenvyl on 2/27/17.
 */

public class Dictionary {
    Context context;
    public HashMap<String, WordTrie> dictionary;
    private static final String TAG = "Dictionary";

    public Dictionary(Context context) {
        Log.d(TAG, "Dictionary constructor");
        this.context = context;
        this.dictionary = new HashMap<>();
    }

    // Loads the key's corresponding .txt file into a trie and
    // adds it to our dictionary.
    public void loadTxtToTrie(String key){
        Log.d(TAG, "loadTxtToTrie");

        try {
            WordTrie newTrie = buildTrie(key);
            addTrie(newTrie);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // If the .txt file hasn't already been loaded into a trie, calls loadTxtToTrie.
    // Otherwise, does nothing. Prevents multiple .txt loads from happening.
    public void loadDict(String word){
        Log.d(TAG, "loadDict");
        String key = checkResName(word.substring(0, 3));
        if(!containsKey(key)){
            Log.d(TAG, "Key " + key + " has not been loaded yet!");
            loadTxtToTrie(key);
        }
    }

    // Searches for a word, also checking that the word's Trie has already been loaded.
    public Boolean search(String word){
        Log.d(TAG, "Searching dict for word " + word);
        String key = checkResName(word.substring(0, 3));
        loadDict(key);
        WordTrie currentTrie = getTrie(key);
        return currentTrie.search(word);
    }

    // Retrieves the corresponding Trie for a given key.
    public WordTrie getTrie(String key){
        return this.dictionary.get(key);
    }


    // Makes sure a resource name doesn't conflict with a Java keyword.
    public static String checkResName(String name){
        if(name.equals("new")){
            name = "new1";
        }
        if(name.equals("try")){
            name = "try1";
        }
        if(name.equals("int")){
            name = "int1";
        }
        if(name.equals("for")){
            name = "for1";
        }
        return name;
    }

    // Builds the word trie out of a .txt file.
    // If the key doesn't have a corresponding .txt file, returns an empty trie.
    public WordTrie buildTrie(String key) throws IOException {
        Log.d(TAG, "Building tree for " + key);

        key = checkResName(key);

        Resources resources = this.context.getApplicationContext().getResources();
        String packageName = this.context.getPackageName();

//        System.out.println("Building tree...");
        WordTrie newTrie = new WordTrie();
        newTrie.setKey(key);
        String currentString;
        String resName = "raw/" + key;
//        System.out.println("Reading file " + resName);
        int resId = resources.getIdentifier(resName, null, packageName);
        // Check if the resource exists.
        if(resId == 0){
            return newTrie;
        }
        InputStream input = resources.openRawResource(resId);
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            if(input != null){
                while((currentString = reader.readLine()) != null){
                    newTrie.insert(currentString);
                }
            }
        } finally {
            try {
                input.close();
                System.out.println("Trie built!");
            }
            catch (Throwable ignore){
                System.out.println("Had trouble reading your file!");
            }
        }
        return newTrie;
    }

    // Puts a word trie into the hashmap of tries.
    public void addTrie(WordTrie trie){
        Log.d(TAG, "addTrie");
        this.dictionary.put(trie.getKey(), trie);
    }

    // Checks if our dictionary has already built the trie for a given key.
    public boolean containsKey(String key){
        Log.d(TAG, "containsKey");
        return this.dictionary.containsKey(key);
    }


}
