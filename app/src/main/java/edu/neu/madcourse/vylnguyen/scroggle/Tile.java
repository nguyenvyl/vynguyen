package edu.neu.madcourse.vylnguyen.scroggle;

import android.graphics.Color;
import android.util.Log;
import android.view.View;

import static edu.neu.madcourse.vylnguyen.R.drawable.tile;

/**
 * Created by nguyenvyl on 2/24/17.
 */

public class Tile {
    private String letter;
    private Integer value;
    private Boolean finalized;
    private View tileView;
    // Levels: 0 = active, 1 = selected, 2 = submitted, 3 = inactive
    private Integer level;
    public SmallBoard parent;
    private Integer position;
    static private final String TAG = "Tile";
    static private final String ACTIVE = "#7f7fff";
    static private final String SELECTED = "#7f007f";
    static private final String SUBMITTED = "#7fbf7f";
    static private final String INACTIVE = "#bfbfbf";



    // Buttons have 4 different levels: Active, Selected, Submitted, and Disabled
    // Need an onClick listener that can rerender the button depending on what state it's in
    // Check out Android level-list in XML


    public Tile(SmallBoard parent, Integer position) {
//        Log.d(TAG, "Tile constructor");
        this.position = position;
        this.parent = parent;
        this.finalized = false;
        this.level = 0;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
        setValue();
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }


    public Integer getValue() {
        return this.value;
    }

    public View getTileView() {
        return tileView;
    }

    public void setTileView(View tileView) {
        this.tileView = tileView;
    }

    public Boolean getFinalized() {
        return finalized;
    }

    public void setFinalized(Boolean finalized) {
        this.finalized = finalized;
    }


    public void onTileClick(View tileView){
        Log.d(TAG, "onTileClick. Current level is " + this.getLevel().toString());
        if(finalized){
            return;
        }
        else if(this.level == 0){
            this.setLevel(1);
            tileView.setBackgroundColor(parseColor(SELECTED));
        }
//        else if(this.level == 1){
//            this.setLevel(0);
//            tileView.setBackgroundColor(parseColor(ACTIVE));
//        }
//        Log.d(TAG, "onTileClick. Changed tile level to " + this.getLevel().toString());

    }

    public void finalizeTile(View tileView){
        this.finalized = true;
        tileView.setClickable(false);
        if(this.level == 1){
            this.level = 2;
            tileView.setBackgroundColor(parseColor(SUBMITTED));
        }
        else{
            this.level = 3;
            tileView.setBackgroundColor(parseColor(INACTIVE));
        }
    }

    public void disableTile(View tileView){
        tileView.setBackgroundColor(parseColor(INACTIVE));
        tileView.setClickable(false);
        this.level = 3;
    }

    public void enableTile(View tileView){
//        Log.d(TAG, "Enabling tile...");
        tileView.setClickable(true);
        tileView.setBackgroundColor(parseColor(ACTIVE));
        this.level = 0;
        this.finalized = false;
    }

    public static int parseColor(String colorString){
        Color color = new Color();
        return color.parseColor(colorString);
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public void setValue(){
        Integer value = 0;
        switch(this.letter){
            case "a":
                value = 1;
                break;
            case "b":
                value = 3;
                break;
            case "c":
                value = 3;
                break;
            case "d":
                value = 2;
                break;
            case "e":
                value = 1;
                break;
            case "f":
                value = 4;
                break;
            case "g":
                value = 2;
                break;
            case "h":
                value = 4;
                break;
            case "i":
                value = 1;
                break;
            case "j":
                value = 8;
                break;
            case "k":
                value = 5;
                break;
            case "l":
                value = 1;
                break;
            case "m":
                value = 3;
                break;
            case "n":
                value = 1;
                break;
            case "o":
                value = 1;
                break;
            case "p":
                value = 3;
                break;
            case "q":
                value = 10;
                break;
            case "r":
                value = 1;
                break;
            case "s":
                value = 1;
                break;
            case "t":
                value = 1;
                break;
            case "u":
                value = 1;
                break;
            case "v":
                value = 4;
                break;
            case "w":
                value = 4;
                break;
            case "x":
                value = 8;
                break;
            case "y":
                value = 4;
                break;
            case "z":
                value = 10;
                break;
        }
        this.value = value;
//        Log.d(TAG, "value set to " + this.value.toString());

    }


}