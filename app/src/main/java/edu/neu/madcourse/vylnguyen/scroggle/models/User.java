package edu.neu.madcourse.vylnguyen.scroggle.models;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static edu.neu.madcourse.vylnguyen.R.string.score;

/**
 * Created by nguyenvyl on 3/6/17.
 */

public class User {
    public String username;
    public ArrayList<Score> scores;
    public String clientToken;

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", scores=" + scores +
                ", clientToken='" + clientToken + '\'' +
                '}';
    }

    public User(){
    }

    public User(ArrayList<Score> scores) {
        this.scores = scores;

    }

    public User(String username, ArrayList<Score> scores, String clientToken) {
        this.username = username;
        this.scores = scores;
        this.clientToken = clientToken;
    }

    public String getClientToken() {
        return clientToken;
    }

    public void setClientToken(String clientToken) {
        this.clientToken = clientToken;
    }

    public ArrayList<Score> getScores() {
        return scores;
    }

    public void setScore(ArrayList<Score> scores) {
        this.scores = scores;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
