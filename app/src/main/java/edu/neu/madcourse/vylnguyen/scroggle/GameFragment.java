/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband4 for more book information.
 ***/
package edu.neu.madcourse.vylnguyen.scroggle;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;

import android.content.DialogInterface;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

import edu.neu.madcourse.vylnguyen.R;
import edu.neu.madcourse.vylnguyen.Singleton;
import edu.neu.madcourse.vylnguyen.scroggle.models.Score;
import edu.neu.madcourse.vylnguyen.scroggle.models.User;


public class GameFragment extends Fragment {
    private static final String TAG = "GameFragment";

    public View fragmentView;
    public LargeBoard largeBoard;
    public CountDownTimer timer;
    public long timeRemaining;
    private static final Integer PHASE_TIME = 10000;
    public Boolean paused = false;
    private String userKey;
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public void setPhase(Integer phase) {
        this.phase = phase;
    }

    private Integer phase;

    static private int smallBoardIds[] = {R.id.scrog_large1, R.id.scrog_large2, R.id.scrog_large3,
            R.id.scrog_large4, R.id.scrog_large5, R.id.scrog_large6, R.id.scrog_large7, R.id.scrog_large8,
            R.id.scrog_large9,};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");

        super.onCreate(savedInstanceState);
        // Retain this fragment across configuration changes.
        setRetainInstance(true);
        this.phase = new Integer(1);
        this.setLargeBoard(new LargeBoard(this, this.getView()));
    }

    public void setLargeBoard(LargeBoard largeBoard) {
        this.largeBoard = largeBoard;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.scroggle_fragment_game, container, false);
        this.fragmentView = rootView;
        initViews();
        this.timer = initTimer(PHASE_TIME).start();
        return rootView;
    }

    private void initViews() {
        this.largeBoard.initSmallBoardViews(this.fragmentView);
        View clear = this.fragmentView.findViewById(R.id.clear_button);
        View submit= this.fragmentView.findViewById(R.id.submit_button);
        final TextView score = (TextView) this.fragmentView.findViewById(R.id.score_textview);

        final LargeBoard gameBoard = this.largeBoard;
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gameBoard.onClear();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer newScore = largeBoard.onSubmit();
                score.setText("Score: " + newScore.toString());
            }
        });
    }



    public void restartGame(){
//        mSoundPool.play(mSoundRewind, mVolume, mVolume, 1, 0, 1f);
        // ...
        this.setLargeBoard(new LargeBoard(this, this.getView()));
        this.largeBoard.restartGame(this.getView());
        initViews();

        // Reset the timer
        resetTimer();

        // Reset the phase
        final TextView phase = (TextView) this.fragmentView.findViewById(R.id.phase_text);
        phase.setText("Phase: 1");
        this.phase = 1;
    }

    public void resetTimer(){
        this.timer.cancel();
        this.phase = 1;
        Singleton.getInstance().setTimeRemaining(PHASE_TIME);
        this.timer = initTimer(PHASE_TIME).start();
    }

    public CountDownTimer initTimer(long time) {
        Log.d("CountDownTimer", "init timer");
        Log.d("CountDownTimer", "starting timer with time " + Long.toString(time));
        final GameFragment fragment = this;
        final TextView timer = (TextView) this.fragmentView.findViewById(R.id.timer_text);
        final TextView phase = (TextView) this.fragmentView.findViewById(R.id.phase_text);
        return new CountDownTimer(time, 1000) {
            public void onTick(long millisUntilFinished) {
                Log.d("onTick", "sec until finished " + Long.toString(millisUntilFinished/1000));
                long timeRemaining = millisUntilFinished / 1000;
                timer.setText("Time remaining: " + timeRemaining);
                Singleton.getInstance().setTimeRemaining(timeRemaining);
            }

            public void onFinish() {
                Log.d(TAG, "on Finish");
                Log.d(TAG, fragment.paused.toString());
                if(fragment.paused.equals(false)){
                    Log.d(TAG, "Timer on finish");
                    if(fragment.phase.equals(1)){
                        Log.d(TAG, "Currently on phase 1");
                        fragment.setPhase(2);
                        phase.setText("Phase: 2");
                        fragment.largeBoard.startPhase2();
                        initViewsPhase2();
                        this.cancel();
                        this.start();
                    }
                    else{
                        Log.d(TAG, "Currently on phase 2, stopping timer");
                        phase.setText("Game complete!");
                        this.cancel();
                        saveScore();
                        reportScore();
                    }
                } else {
                    Log.d(TAG, "onFinish - game paused");
//                    this.cancel();
                }

            }
        };
    }

    public void initViewsPhase2(){
        View clear = this.fragmentView.findViewById(R.id.clear_button);
        View submit= this.fragmentView.findViewById(R.id.submit_button);

        final TextView score = (TextView) this.fragmentView.findViewById(R.id.score_textview);

        final LargeBoard gameBoard = this.largeBoard;
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gameBoard.onClearPhase2();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer newScore = largeBoard.onSubmitPhase2();
                score.setText("Score: " + newScore.toString());
            }
        });
    }

    public void saveScore(){
        Log.d(TAG, "Save score");
        Integer score = largeBoard.score;
        String bestWord = largeBoard.bestWord;
        Integer wordScore = largeBoard.bestWordScore;

        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yy HH:mm");
        Calendar calendar = Calendar.getInstance();
        String date = df.format(calendar.getTime());

        Score newScore = new Score(score, bestWord, wordScore, date);

        addScore(Singleton.getInstance().getmDatabase(), userKey, newScore);
        checkHighScore(Singleton.getInstance().getmDatabase(), newScore);
    }



    // Check if the score is a new high score.
    public void checkHighScore(DatabaseReference postRef, final Score newScore){
        Log.d(TAG, "checkHighScore");
        postRef
                .child("TopScores")
                .child("Score")
                .runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        Score bestScore = mutableData.getValue(Score.class);
                        if (bestScore == null) {
                            Log.d(TAG, "no best score yet");
                            mutableData.setValue(bestScore);
                            new SendNotification(newScore).execute();
                        } else {
                            Log.d(TAG, "Current best score: " + bestScore.toString());
                            if (newScore.getScore() > bestScore.getScore()) {
                                Log.d(TAG, "Changing best score to " + newScore.getScore().toString());
                                mutableData.setValue(newScore);
                                new SendNotification(newScore).execute();
                            }
                        }
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b,
                                           DataSnapshot dataSnapshot) {
                        // Transaction completed
                        Log.d(TAG, "postTransaction:onComplete:" + databaseError);
                    }
                });
    }


    private class SendNotification extends AsyncTask<Void, Integer, Void> {
        private Score score;

        SendNotification(Score score){
            Log.d("SendNotification", "Send notification constructor");
            this.score = score;
        }

        @Override
        protected Void doInBackground(Void... params) {
            String currentUser = Singleton.getInstance().getUsername();
            Log.d(TAG, "pushNotification private");

            JSONObject jPayload = new JSONObject();
            JSONObject jNotification = new JSONObject();
            try {
                jNotification.put("title", "New High Score!");
                jNotification.put("body", currentUser + " set a new record of " + score.getScore().toString() + "!");
                jNotification.put("sound", "default");
                jNotification.put("badge", "1");
                jNotification.put("click_action", "OPEN_ACTIVITY_1");

                jPayload.put("to", "/topics/news");

                jPayload.put("priority", "high");
                jPayload.put("notification", jNotification);

                URL url = new URL("https://fcm.googleapis.com/fcm/send");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Authorization", Singleton.getServerKey());
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setDoOutput(true);

                // Send FCM message content.
                OutputStream outputStream = conn.getOutputStream();
                outputStream.write(jPayload.toString().getBytes());
                outputStream.close();
                // Read FCM response.
                InputStream inputStream = conn.getInputStream();
                final String resp = convertStreamToString(inputStream);
                Log.d("SendNotification", "Response: " + resp);
            } catch (JSONException | IOException e) {
                Log.d("SendNotification", e.toString());
                e.printStackTrace();
            }
            return null;
        }

        private String convertStreamToString(InputStream is) {
            Log.d(TAG, "convertStreamToString");

            Scanner s = new Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next().replace(",", ",\n") : "";
        }
    }

    // Add a score to a specified user
    public void addScore(DatabaseReference postRef, String userKey, Score newScore){
        Log.d(TAG, "Adding new score to user " + username);
        Log.d(TAG, newScore.toString());
        final Score score = newScore;
        postRef
                .child("Users")
                .child(userKey)
                .runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        User user = mutableData.getValue(User.class);
                        Log.d(TAG, "Old user: " + user.toString());
                        if (user == null) {
                            return Transaction.success(mutableData);
                        }

                        ArrayList<Score> newScoreList = user.getScores();
                        if(newScoreList == null){
                            newScoreList = new ArrayList<>();
                        }
                        newScoreList.add(score);
                        user.setScore(newScoreList);
                        Log.d(TAG, "updated user: " + user.toString());

                        mutableData.setValue(user);
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b,
                                           DataSnapshot dataSnapshot) {
                        // Transaction completed
                        Log.d(TAG, "postTransaction:onComplete:" + databaseError);
                    }
                });
    }

    public void reportScore(){
        Log.d(TAG, "report score");
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        final Activity gameActivity = this.getActivity();
        String message = "You scored: " + this.largeBoard.score.toString();
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.OK,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        gameActivity.finish();
                    }
                });
        final Dialog dialog = builder.create();
        dialog.show();
    }


}

