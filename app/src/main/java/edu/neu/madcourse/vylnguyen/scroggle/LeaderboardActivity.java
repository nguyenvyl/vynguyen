package edu.neu.madcourse.vylnguyen.scroggle;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import edu.neu.madcourse.vylnguyen.Singleton;
import edu.neu.madcourse.vylnguyen.scroggle.models.Score;
import edu.neu.madcourse.vylnguyen.scroggle.models.User;
import edu.neu.madcourse.vylnguyen.R;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

public class LeaderboardActivity extends AppCompatActivity {

    static private int rankIds[] = {R.id.rank_1, R.id.rank_2, R.id.rank_3,
            R.id.rank_4, R.id.rank_5, R.id.rank_6, R.id.rank_7, R.id.rank_8,
            R.id.rank_9, R.id.rank_10};
    static private int dateIds[] = {R.id.date_1, R.id.date_2, R.id.date_3,
            R.id.date_4, R.id.date_5, R.id.date_6, R.id.date_7, R.id.date_8,
            R.id.date_9, R.id.date_10};
    static private int scoreIds[] = {R.id.score_1, R.id.score_2, R.id.score_3,
            R.id.score_4, R.id.score_5, R.id.score_6, R.id.score_7, R.id.score_8,
            R.id.score_9, R.id.score_10};
    static private int bestWordIds[] = {R.id.best_word_1, R.id.best_word_2, R.id.best_word_3,
            R.id.best_word_4, R.id.best_word_5, R.id.best_word_6, R.id.best_word_7, R.id.best_word_8,
            R.id.best_word_9, R.id.best_word_10};
    static private int bestWordScoreIds[] = {R.id.best_word_score_1, R.id.best_word_score_2, R.id.best_word_score_3,
            R.id.best_word_score_4, R.id.best_word_score_5, R.id.best_word_score_6, R.id.best_word_score_7, R.id.best_word_score_8,
            R.id.best_word_score_9, R.id.best_word_score_10};
    static private int usernameIds[] = {R.id.username_1, R.id.username_2, R.id.username_3,
            R.id.username_4, R.id.username_5, R.id.username_6, R.id.username_7, R.id.username_8,
            R.id.username_9, R.id.username_10};


    private static final String TAG = LeaderboardActivity.class.getSimpleName();
    public ArrayList<User> mUsers = new ArrayList<>();
    public ArrayList<User> bestScores = new ArrayList<>();


    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scroggle_activity_leaderboard);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("Users").addChildEventListener(
                new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Log.d(TAG, dataSnapshot.getChildren().toString());
                        User user = dataSnapshot.getValue(User.class);
                        mUsers.add(user);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        Log.e(TAG, "onChildRemoved: dataSnapshot = " + dataSnapshot.getValue());

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                        Log.e(TAG, "onChildMoved: dataSnapshot = " + dataSnapshot.getValue());

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, "onCancelled:" + databaseError);
                    }
                }
        );
        mDatabase.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d(TAG, "onDataChange");
                        getBestScores();
                        populateUsers();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );

    }

    private void populateUsers(){
        Log.d(TAG, "populateUsers");
        int size = 10;
        if(bestScores.size() < 10){
            size = bestScores.size();
        }

        for(int i = 0; i < size; i++){
            TextView rankView = (TextView) findViewById(rankIds[i]);
            TextView usernameView = (TextView) findViewById(usernameIds[i]);
            TextView dateView = (TextView) findViewById(dateIds[i]);
            TextView scoreView = (TextView) findViewById(scoreIds[i]);
            TextView bestWordView = (TextView) findViewById(bestWordIds[i]);
            TextView bestWordScoreView = (TextView) findViewById(bestWordScoreIds[i]);

            rankView.setText(Integer.toString(i + 1));
            usernameView.setText(bestScores.get(i).getUsername());
            Score bestScore = bestScores.get(i).getScores().get(0);
            dateView.setText(bestScore.getDate());
            scoreView.setText(bestScore.getScore().toString());
            bestWordView.setText(bestScore.getBestWord());
            bestWordScoreView.setText(bestScore.getWordScore().toString());

        }
    }


    private void getBestScores(){
        Log.d(TAG, "getBestScores");
        for(int i = 0; i < mUsers.size(); i++){
            Log.d(TAG, "get best score, checking user " + mUsers.get(i).getUsername());
            ArrayList<Score> scores = mUsers.get(i).getScores();
            if(scores != null){
                Log.d(TAG, scores.toString());
                for(int j = 0; j < scores.size(); j++){
                    Log.d(TAG, "score: " + scores.get(j).getScore().toString());
                    String username = mUsers.get(i).getUsername();
                    ArrayList<Score> bestScore = new ArrayList<>();
                    bestScore.add(scores.get(j));
                    bestScores.add(new User(username, bestScore, ""));
                }
            }
        }
        sortBestScores();
    }


    private void sortBestScores(){
        Log.d(TAG, "sortBestScores");
        Collections.sort(this.bestScores, new Comparator<User>() {
            @Override
            public int compare(User user1, User user2) {
                Integer user1Score = user1.getScores().get(0).getScore();
                Integer user2Score = user2.getScores().get(0).getScore();
                return user2Score - user1Score;
            }
        });
    }
}
