/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband4 for more book information.
 ***/
package edu.neu.madcourse.vylnguyen.scroggle;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import edu.neu.madcourse.vylnguyen.R;
import edu.neu.madcourse.vylnguyen.Singleton;
import edu.neu.madcourse.vylnguyen.scroggle.models.Score;
import edu.neu.madcourse.vylnguyen.scroggle.models.User;


public class MainActivity extends Activity {

    MediaPlayer mMediaPlayer;
    private DatabaseReference mDatabase;
    protected Integer numUsers = new Integer(0);
    private String currentUserKey;
    private String currentUsername;
    private Boolean userExists = false;
    private static final String USER_KEY = "user_key";
    private static final String USERNAME = "username";
    private static final String TAG = "Scroggle MainActivity";
    // ...

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "hi from on create");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scroggle_main);
        read9Letters();
        loadFirebase();
    }

    public void read9Letters(){
        int resId = getResources().getIdentifier("nine_letters", "raw", getPackageName());
        InputStream input = getResources().openRawResource(resId);
        String currentString;
        ArrayList<String> words = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            if(input != null){
                while((currentString = reader.readLine()) != null){
                    words.add(currentString);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                input.close();
                Log.i(TAG, "All 9 letter words added to array!");
            }
            catch (Throwable ignore){
                Log.e(TAG, "Had trouble reading your file!");
            }
        }
        Singleton.getInstance().set9letters(words);
    }


    public void loadFirebase(){
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d(TAG, "Number of users: " + numUsers.toString());

                        if(!userExists()){
                            Log.d(TAG, "user doesn't exist, creating new user...");
                            createNewUser();
                        }
                        else{
                            Log.d(TAG, "user exists, retrieving user...");
                            setSavedUser();
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                }
        );

        mDatabase.child("Users").addChildEventListener(
                new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        numUsers++;
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        Log.e(TAG, "onChildRemoved: dataSnapshot = " + dataSnapshot.getValue());

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                        Log.e(TAG, "onChildMoved: dataSnapshot = " + dataSnapshot.getValue());

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, "onCancelled:" + databaseError);
                    }
                }
        );
        Singleton.getInstance().setmDatabase(mDatabase);
    }


    private boolean userExists(){
        return PreferenceManager.getDefaultSharedPreferences(this.getApplication()).contains(USER_KEY);
    }

    private String getUserKey(){
        return PreferenceManager.getDefaultSharedPreferences(this.getApplication()).getString(USER_KEY, "");
    }

    private String getUsername(){
        return PreferenceManager.getDefaultSharedPreferences(this.getApplication()).getString(USERNAME, "");
    }

    public void setSavedUser(){
        Log.d(TAG, "set saved user");
        String userKey = getUserKey();
        String username = getUsername();
        Singleton.getInstance().setUserKey(userKey);
        Singleton.getInstance().setUsername(username);
    }

    public void createNewUser(){
        Log.d(TAG, "create new user");
        Integer userKey = numUsers + 1;
        String username = "user" + userKey.toString();
        Log.d(TAG, "creating user key " + userKey + " with username " + username);
        PreferenceManager.getDefaultSharedPreferences(this.getApplication()).edit()
                .putString(USER_KEY, userKey.toString())
                .putString(USERNAME, username)
                .commit();
        Singleton.getInstance().setUserKey(userKey.toString());
        Singleton.getInstance().setUsername(username);
        pushUserToDatabase(mDatabase, userKey.toString(), username);
        // Subscribe to high score alerts
        FirebaseMessaging.getInstance().subscribeToTopic("news");
    }


    public void pushUserToDatabase(DatabaseReference postRef, String userKey, String username){
        this.currentUsername = username;
        ArrayList<Score> scores = new ArrayList<>();
        String token = FirebaseInstanceId.getInstance().getToken();
        final User newUser = new User(username, scores, token);
        Log.d(TAG, "Adding user " + newUser.toString());
        postRef
                .child("Users")
                .child(userKey)
                .runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                            mutableData.setValue(newUser);
                            return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b,
                                           DataSnapshot dataSnapshot) {
                        // Transaction completed
                        Log.d(TAG, "postTransaction:onComplete:" + databaseError);
                    }
                });
    }


    // Resumes the activity
    @Override
    protected void onResume() {
        super.onResume();
        mMediaPlayer = MediaPlayer.create(this, R.raw.ynwainstrumental);
        mMediaPlayer.setVolume(0.5f, 0.5f);
        mMediaPlayer.setLooping(true);
        mMediaPlayer.start();
    }

    // Pauses the game and stops the music
    @Override
    protected void onPause() {
        super.onPause();
        mMediaPlayer.stop();
        mMediaPlayer.reset();
        mMediaPlayer.release();
    }
}
