package edu.neu.madcourse.vylnguyen.scroggle;

import android.app.Activity;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Set;

import edu.neu.madcourse.vylnguyen.R;
import edu.neu.madcourse.vylnguyen.Singleton;

import static android.R.transition.move;
import static edu.neu.madcourse.vylnguyen.R.raw.cur;
import static edu.neu.madcourse.vylnguyen.R.raw.i;
import static edu.neu.madcourse.vylnguyen.R.raw.ret;
import static edu.neu.madcourse.vylnguyen.R.raw.t;
import static edu.neu.madcourse.vylnguyen.R.raw.tho;

/**
 * Created by nguyenvyl on 2/24/17.
 */

public class SmallBoard {

    private ArrayList<Tile> tiles = new ArrayList<Tile>();
    private StringBuilder currentString;
    private Boolean finalized;
    private Boolean isActive;
    public LargeBoard parent;
    public Integer position;
    public Integer lastMove;


    private View smallBoardView;
    static private int tileIds[] = {R.id.scrog_small1, R.id.scrog_small2, R.id.scrog_small3,
            R.id.scrog_small4, R.id.scrog_small5, R.id.scrog_small6, R.id.scrog_small7, R.id.scrog_small8,
            R.id.scrog_small9,};
    static private final String TAG = "SmallBoard";
    // private LargeBoard largeBoard;

    public void setSmallBoardView(View smallBoardView) {
        this.smallBoardView = smallBoardView;
    }

    public SmallBoard(String word, LargeBoard parent, Integer position) {
        this.position = position;
        this.parent = parent;
        this.finalized = false;
        this.isActive = true;
        this.currentString = new StringBuilder();
        this.lastMove = new Integer(-1);
        initBoard(word);
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    /*************************************************************************************
     * Code for Phase 1
     **************************************************************************************/

    public void initBoard(String word){
        // Add the tiles
        for(int i = 0; i < 9; i++){
            this.tiles.add(new Tile(this, i));
        }
        Graph graph = new Graph();
        ArrayList<Integer> layout = graph.generateLayout();
        for(int i = 0; i < 9; i++){
            String currentLetter = Character.toString(word.charAt(i));
            Integer addIndex = layout.get(i);
            this.tiles.get(addIndex).setLetter(currentLetter);
        }
    }

    // Sets the button views.
    public void initTileViews(){
        for(int i = 0; i < 9; i++){
//            final String currentI = Integer.toString(i);
//            Log.d(TAG, "Setting tile view number " + currentI);

            final Integer currentTilePosition = i;
            final Button tileView = (Button) this.smallBoardView.findViewById(tileIds[i]);
            final Tile currentTile = tiles.get(i);
            final SmallBoard currentBoard = this;
            final LargeBoard parent = this.parent;

            tileView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            Log.d("You move", "Tile " + currentI);

                            // If a valid neighbor is move, select the tile.
                            if(currentBoard.isValidMove(currentTilePosition)){
                                currentBoard.doMove(currentTile, currentTilePosition);
                                currentTile.onTileClick(tileView);
                            }
                            // Load the dictionary as the user types.
                            if(currentString.length() >= 3){
                                loadDict(currentString);
                            }
                            // Set this board to active
                            currentBoard.isActive = true;
                            parent.onClick(currentBoard.position);
                        }
                    }
            );
            tileView.setText(this.tiles.get(i).getLetter());
            tiles.get(i).setTileView(tileView);
        }
    }

    public void doMove(Tile currentTile, Integer move){
        this.addLetter(currentTile.getLetter());
        this.lastMove = move;
    }

    public void addLetter(String letter){
        this.currentString.append(letter);
    }

    public void restartBoard(View smallBoardView){
        Boolean thisView = smallBoardView == null;
//        Log.d(TAG, this)
        this.finalized = false;
        this.clearCurrentString();
        this.lastMove = -1;
        for(int i = 0; i < 9; i++){
            final Button tileView = (Button) smallBoardView.findViewById(tileIds[i]);
            this.tiles.get(i).enableTile(tileView);
        }
    }

    public static void loadDict(StringBuilder word){
        Singleton.getInstance().getDictionaryClass().loadDict(word.toString());
    }

    public Boolean isValidMove(Integer move){
//        Log.d(TAG, "isValidMove");
        if(this.lastMove.equals(-1)){
//            Log.d(TAG, "No tiles selected yet, move is valid");
            return true;
        }
        return (isNeighbor(move) && !isLastMove(move) && !isSelected(move));
    }

    public Boolean isSelected(Integer move){
        Tile tile = this.tiles.get(move);
        return tile.getLevel().equals(1);
    }

    public Boolean isNeighbor(Integer move){
        Graph graph = new Graph();
        ArrayList<Integer> neighbors = graph.getNeighbors(this.lastMove);
//        Log.d(TAG, "isNeighbor?");
//        Log.d(TAG, Boolean.toString(neighbors.contains(move)));
        return neighbors.contains(move);
    }

    public Boolean isLastMove(Integer move){
        return this.lastMove.equals(move);
    }


    public void disableBoard(){
        for(int i = 0; i < 9; i++){
            if(!finalized) {
                final Tile currentTile = tiles.get(i);
                final Button tileView = (Button) this.smallBoardView.findViewById(tileIds[i]);
                currentTile.disableTile(tileView);
            }
        }
        this.isActive = false;
    }


    public StringBuilder getCurrentString() {
        return currentString;
    }

    public void setCurrentString(StringBuilder currentString) {
        this.currentString = currentString;
    }

    public Integer submitBoard(){
        final Activity currentActivity = this.parent.getParent().getActivity();
        final MediaPlayer validWordSound = new MediaPlayer().create(currentActivity, R.raw.valid_word);
        final MediaPlayer invalidWordSound = new MediaPlayer().create(currentActivity, R.raw.invalid_word);

        String word = this.currentString.toString();
        Integer currentWordScore = LargeBoard.scoreWord(word);
        Boolean isValid = Singleton.getInstance().getDictionaryClass().search(word);
        if(isValid){
            finalizeBoard();
            this.parent.updateBestWord(word, currentWordScore);
            validWordSound.start();
            return currentWordScore;
        }
        else {
            clearCurrentString();
            enableBoard();
            invalidWordSound.start();
            return currentWordScore * -1;
        }
    }

    public void clearCurrentString(){
        this.currentString.setLength(0);
        this.currentString.trimToSize();
    }

    // Finalizes a board if its submitted word is valid.
    // Sets all selected tiles to submitted and changes this.finalized = true so that
    // the player can no longer change it.
    public void finalizeBoard(){
        for(int i = 0; i < 9; i++){
            Button tileView = (Button) this.smallBoardView.findViewById(tileIds[i]);
            Tile currentTile = tiles.get(i);
            currentTile.finalizeTile(tileView);
            this.isActive = false;
            this.finalized = true;
        }
    }

    public void enableBoard(){
//        Log.d(TAG, "enableBoard");
        if(!this.finalized){
//            Log.d(TAG, "Board is not finalized, re-enabling board");
            for(int i = 0; i < 9; i++){
                final Tile currentTile = tiles.get(i);
                final Button tileView = (Button) this.smallBoardView.findViewById(tileIds[i]);
                currentTile.enableTile(tileView);
            }
            clearCurrentString();
            this.isActive = false;
            this.lastMove = -1;
        }
    }

    /*************************************************************************************
     * Code for Phase 2
     **************************************************************************************/

    public void startPhase2(){
        if(!this.finalized){
            disableBoardPermanent();
        }
        else {
            clearCurrentString();
            this.isActive = false;
            this.finalized = false;
            this.lastMove = -1;
            for(int i = 0; i < 9; i++){
                final Tile currentTile = this.tiles.get(i);
                final Button tileView = (Button) this.smallBoardView.findViewById(tileIds[i]);
                if(currentTile.getLevel() == 2){
                    currentTile.enableTile(tileView);
                } else {
                    currentTile.disableTile(tileView);
                }

                final SmallBoard currentBoard = this;
                final Integer currentTilePosition = i;
                tileView.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if(currentBoard.isValidPhase2Move(currentTilePosition)){
                                    currentBoard.doPhase2Move(currentTile);
                                    currentTile.onTileClick(tileView);
                                }
                            }
                        }
                );
            }
        }
    }

    public void disableBoardPermanent(){
        for(int i = 0; i < 9; i++){
            if(!finalized) {
                final Tile currentTile = tiles.get(i);
                final Button tileView = (Button) this.smallBoardView.findViewById(tileIds[i]);
                currentTile.disableTile(tileView);
            }
        }
        this.finalized = true;
        this.isActive = false;
    }

    public void phase2SubmitTiles(){
        for(int i = 0; i < 9; i++) {
            Button tileView = (Button) this.smallBoardView.findViewById(tileIds[i]);
            Tile currentTile = tiles.get(i);
            if(currentTile.getLevel().equals(1)){
                currentTile.finalizeTile(tileView);
            }
        }
    }

    public void enableBoardPhase2(){
//        Log.d(TAG, "enableBoardPhase2");
        if(!this.finalized){
            for(int i = 0; i < 9; i++){
                final Tile currentTile = tiles.get(i);
                final Button tileView = (Button) this.smallBoardView.findViewById(tileIds[i]);
                if(!isFinalized(i)){
                    currentTile.enableTile(tileView);
                }
            }
        }
    }

    public Boolean isValidPhase2Move(Integer move){
        return !isSelected(move) && !isFinalized(move);
    }

    public Boolean isFinalized(Integer move){
        Tile currentTile = this.tiles.get(move);
        return currentTile.getFinalized();
    }

    public void doPhase2Move(Tile currentTile){
//        Log.d(TAG, "doPhase2Move");
        Integer move = currentTile.getPosition();
//        Log.d(TAG, "Move: " + move.toString());
        this.parent.onPhase2Click(currentTile);
        for(int i = 0; i < 9; i++){
            Tile tile = this.tiles.get(i);
            if(!move.equals(i) && !tile.getFinalized()){
                Button tileView = (Button) this.smallBoardView.findViewById(tileIds[i]);
                tile.disableTile(tileView);
            }
        }
    }

    public String getState(){
        return "";
    }

    public void putState(){

    }





}
