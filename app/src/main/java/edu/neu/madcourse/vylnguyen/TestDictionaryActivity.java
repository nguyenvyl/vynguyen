package edu.neu.madcourse.vylnguyen;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import edu.neu.madcourse.vylnguyen.dictionary.WordTrie;

public class TestDictionaryActivity extends AppCompatActivity {

    HashSet<String> words = new HashSet<>(); // HashSet for words that have matched user input

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.println("Creating Dictionary Activity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_dictionary);

        EditText input = (EditText) findViewById(R.id.edittext);
        final TextView output = (TextView)findViewById(R.id.outputlist);

        // Set up Media Player for sound.
        final MediaPlayer sound = new MediaPlayer().create(this, R.raw.beep);
        // Text listener for user input.
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String currentString = s.toString();
                // Only search for a word if it's 3 or more letters long
                if(currentString.length() > 2){
                    String firstChar = checkResName(currentString.substring(0, 3));
                    HashMap<String, WordTrie> currentDict = Singleton.getInstance().getDictionary();

                    // If the trie hasn't been built yet, build it and add it to the dictionary.
                    if(!currentDict.containsKey(firstChar)){
                        try {
                            WordTrie newTrie = buildTrie(firstChar);
                            addTrie(newTrie);
                            currentDict = Singleton.getInstance().getDictionary();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    // Search the dictionary for the word.
                    WordTrie currentTrie = currentDict.get(firstChar);
                    if(currentTrie.search(currentString)){
                        words.add(currentString);
                        sound.start();
                    }
                    output.setText(hashSetToString(words));
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
        });
    }
    
    // Makes sure a resource name doesn't conflict with a Java keyword. 
    public String checkResName(String name){
        if(name.equals("new")){
            name = "new1";
        }
        if(name.equals("try")){
            name = "try1";
        }
        if(name.equals("int")){
            name = "int1";
        }
        if(name.equals("for")){
            name = "for1";
        }
        return name;
    }

    // Show the acknowledgements screen.
    public void showAcknowledgments(View view){
        Intent intent = new Intent(this, AcknowledgementsActivity.class);
        startActivity(intent);
    }

    // Show the main screen.
    public void showMain(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        this.finish();
    }

    public void clearWords(View view){
        this.words.clear();
        final TextView output = (TextView)findViewById(R.id.outputlist);
        output.setText(hashSetToString(this.words));
    }

    // Converts a hashset to a string to display to user.
    public StringBuilder hashSetToString(HashSet<String> words){
        Iterator<String> iterator = words.iterator();
        StringBuilder wordString = new StringBuilder();
        if(!words.isEmpty()){
            while(iterator.hasNext()){
                wordString.append(iterator.next());
                wordString.append('\n');
            }
        }
        return wordString;
    }

    // Builds the word trie out of a .txt file.
    public WordTrie buildTrie(String key) throws IOException{
        System.out.println("Building tree...");
        WordTrie newTrie = new WordTrie();
        newTrie.setKey(key);
        String currentString;
        String resName = "raw/" + key;
        System.out.println("Reading file " + resName);
        int resId = getResources().getIdentifier(resName, null, this.getPackageName());
        // Check if the resource exists. 
        if(resId == 0){
            return newTrie;
        }
        InputStream input = getResources().openRawResource(resId);
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            if(input != null){
                while((currentString = reader.readLine()) != null){
                    newTrie.insert(currentString);
                }
            }
        } finally {
            try {
                input.close();
                System.out.println("Trie built!");
            }
            catch (Throwable ignore){
                System.out.println("Had trouble reading your file!");
            }
        }
        return newTrie;
    }

    // Puts a word trie into the hashmap of tries.
    public void addTrie(WordTrie trie){
        Singleton.getInstance().addTrie(trie);
    }


//    // Async class for loading the .txt file of words into a Trie object
//    private class LoadTextDict extends AsyncTask<String, Void, String>{
//        @Override
//        protected String doInBackground(String... params) {
//            System.out.println("Loading dictionary from original .txt file...");
//            try {
//                buildTrie();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return "executed!";
//        }
//        @Override
//        protected void onPostExecute(String result) {}
//        @Override
//        protected void onPreExecute() {}
//        @Override
//        protected void onProgressUpdate(Void... values) {}
//    }

}
