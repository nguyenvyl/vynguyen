package edu.neu.madcourse.vylnguyen.scroggle;

import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

import edu.neu.madcourse.vylnguyen.R;
import edu.neu.madcourse.vylnguyen.Singleton;
import edu.neu.madcourse.vylnguyen.scroggle.models.Score;
import edu.neu.madcourse.vylnguyen.scroggle.models.User;


public class ChangeUsernameActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private static final String TAG = "ChangeUsernameActivity";
    private static final String USER_KEY = "user_key";
    private static final String USERNAME = "username";
    private int numUsers;
    private String currentUsername;
    private String currentUserKey;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scroggle_activity_change_username);

        loadFirebase();

        final EditText input = (EditText) findViewById(R.id.username_input);
        Button submit = (Button) findViewById(R.id.submit_change_username);
        submit.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View view){
                        updateUsername(mDatabase, currentUserKey, input.getText().toString());
                    }
                }
        );
    }

    public void setSavedUser(){
        Log.d(TAG, "set saved user");
        this.currentUserKey= getUserKey();
        this.currentUsername = getUsername();
        setUsernameInputText();
        Log.d(TAG, "user key: " + this.currentUserKey);
        Log.d(TAG, "username: " + this.currentUsername);
    }

    public void loadFirebase() {
        Log.d(TAG, "loadFirebase");

        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d(TAG, "Number of users: " + Integer.toString(numUsers));
                        if(!userExists()){
                            Log.d(TAG, "user doesn't exist, creating new user...");
                            createNewUser();
                        }
                        else{
                            Log.d(TAG, "user exists, retrieving user...");
                            setSavedUser();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );

        mDatabase.child("Users").addChildEventListener(
                new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Log.d(TAG, "onChildAdded");
                        numUsers++;
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        Log.e(TAG, "onChildRemoved: dataSnapshot = " + dataSnapshot.getValue());

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                        Log.e(TAG, "onChildMoved: dataSnapshot = " + dataSnapshot.getValue());

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, "onCancelled:" + databaseError);
                    }
                }
        );
    }

    public void createNewUser(){
        Log.d(TAG, "create new user");
        Integer userKey = numUsers + 1;
        String username = "user" + userKey.toString();
        Log.d(TAG, "creating user key " + userKey + " with username " + username);
        PreferenceManager.getDefaultSharedPreferences(this.getApplication()).edit()
                .putString(USER_KEY, userKey.toString())
                .putString(USERNAME, username)
                .commit();
        this.currentUserKey = userKey.toString();
        this.currentUsername = username;
        Log.d(TAG, "user key: " + this.currentUserKey);
        Log.d(TAG, "username: " + this.currentUsername);

        addNewEmptyUser(mDatabase, userKey.toString(), username);
    }

    public void updateUsername(DatabaseReference postRef, String userKey, String newUsername){
        this.currentUsername = newUsername;
        Singleton.getInstance().setUsername(newUsername);
        setUsernameInputText();
        // Save to shared preferences
        PreferenceManager.getDefaultSharedPreferences(this.getApplication()).edit()
                .putString(USERNAME, newUsername)
                .commit();

        // Save to Firebase
        final String username = newUsername;
        postRef
                .child("Users")
                .child(userKey)
                .runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        User user = mutableData.getValue(User.class);
                        Log.d(TAG, "Old user: " + user.toString());
                        if (user == null) {
                            return Transaction.success(mutableData);
                        }

                        user.setUsername(username);
                        Log.d(TAG, "updated user: " + user.toString());

                        mutableData.setValue(user);

                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b,
                                           DataSnapshot dataSnapshot) {
                        // Transaction completed
                        if(databaseError != null){
                            Toast.makeText(ChangeUsernameActivity.this,
                                    "There was a problem changing your username. Try again in a little bit.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(ChangeUsernameActivity.this,
                                    "Username changed!", Toast.LENGTH_SHORT).show();
                        }

                        Log.d(TAG, "postTransaction:onComplete:" + databaseError);
                    }
                });
    }

    private boolean userExists(){
        return PreferenceManager.getDefaultSharedPreferences(this.getApplication()).contains(USER_KEY);
    }

    private String getUserKey(){
        return PreferenceManager.getDefaultSharedPreferences(this.getApplication()).getString(USER_KEY, "");
    }

    private String getUsername(){
        return PreferenceManager.getDefaultSharedPreferences(this.getApplication()).getString(USERNAME, "");
    }

    private void setUsernameInputText(){
        final EditText input = (EditText) findViewById(R.id.username_input);
        input.setText(currentUsername);
    }

    public void addNewEmptyUser(DatabaseReference postRef, String userKey, String username){
        this.currentUsername = username;
        setUsernameInputText();
        ArrayList<Score> scores = new ArrayList<>();
        String token = FirebaseInstanceId.getInstance().getToken();
        final User newUser = new User(username, scores, token);
        Log.d(TAG, "Adding user " + newUser.toString());
        postRef
                .child("Users")
                .child(userKey)
                .runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        User user = mutableData.getValue(User.class);
                        if (user == null) {
                            Log.d(TAG, "User is null ");
                            mutableData.setValue(newUser);
                            return Transaction.success(mutableData);
                        }
                        Log.d(TAG, "User is...not null? ");
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b,
                                           DataSnapshot dataSnapshot) {
                        // Transaction completed
                        Log.d(TAG, "postTransaction:onComplete:" + databaseError);
                    }
                });
    }


}
