package edu.neu.madcourse.vylnguyen.scroggle;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Scanner;

import edu.neu.madcourse.vylnguyen.R;
import edu.neu.madcourse.vylnguyen.Singleton;
import edu.neu.madcourse.vylnguyen.scroggle.models.User;


public class MessageUserActivity extends AppCompatActivity {

    private HashMap<String, String> users = new HashMap<>();
    private static final String TAG = "MessageUserActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scroggle_activity_message_user);

        final EditText username= (EditText) findViewById(R.id.username_input);
        final EditText message = (EditText) findViewById(R.id.message_input);

        Button submit = (Button) findViewById(R.id.submit_change_username);
        submit.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View view){
                        String usernameToSend = username.getText().toString();
                        String messageToSend = message.getText().toString();
                        String token = getTokenFromUsername(usernameToSend);
                        if(token == null){
                            Toast.makeText(MessageUserActivity.this,
                                    "User not found. Check your spelling or try another username.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else{
                            // send message in async task
                            new SendNotification(Singleton.getInstance().getUsername(), messageToSend, token).execute();
                        }
                    }
                }
        );
        loadFirebase();
    }


    private void loadFirebase(){
        Singleton.getInstance().getmDatabase().child("Users").addChildEventListener(
                new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Log.d(TAG, "onChildAdded");
                        User user = dataSnapshot.getValue(User.class);
                        users.put(user.username, user.clientToken);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        Log.e(TAG, "onChildRemoved: dataSnapshot = " + dataSnapshot.getValue());

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                        Log.e(TAG, "onChildMoved: dataSnapshot = " + dataSnapshot.getValue());

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, "onCancelled:" + databaseError);
                    }
                }
        );

        Singleton.getInstance().getmDatabase().addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d(TAG, users.toString());

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );

    }

    private String getTokenFromUsername(final String username){
        return users.get(username);
    }

    private class SendNotification extends AsyncTask<Void, Integer, Void> {
        private String message;
        private String senderUsername;
        private String token;
        private Boolean error = false;

        SendNotification(String senderUsername, String message, String token) {
            this.message = message;
            this.senderUsername = senderUsername;
            this.token = token;
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.d(TAG, "pushNotification private");

            JSONObject jPayload = new JSONObject();
            JSONObject jNotification = new JSONObject();
            try {
                jNotification.put("title", "New Message!");
                jNotification.put("body", senderUsername + " says: " + message);
                jNotification.put("sound", "default");
                jNotification.put("badge", "1");
                jNotification.put("click_action", "OPEN_ACTIVITY_1");

                jPayload.put("to", token);

                jPayload.put("priority", "high");
                jPayload.put("notification", jNotification);

                URL url = new URL("https://fcm.googleapis.com/fcm/send");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Authorization", Singleton.getServerKey());
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setDoOutput(true);

                // Send FCM message content.
                OutputStream outputStream = conn.getOutputStream();
                outputStream.write(jPayload.toString().getBytes());
                outputStream.close();
                // Read FCM response.
                InputStream inputStream = conn.getInputStream();
                final String resp = convertStreamToString(inputStream);
                Log.d("SendNotification", "Response: " + resp);

            } catch (JSONException | IOException e) {
                Log.d("SendNotification", e.toString());
                error = true;
                e.printStackTrace();
            }
            return null;
        }
        private String convertStreamToString(InputStream is) {
            Log.d(TAG, "convertStreamToString");

            Scanner s = new Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next().replace(",", ",\n") : "";
        }

        protected void onPostExecute(Void result){
            if(error == false){
                Toast.makeText(MessageUserActivity.this,
                        "Message sent!", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(MessageUserActivity.this,
                        "There was a problem sending your message. Try again in a little bit.",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }



}
