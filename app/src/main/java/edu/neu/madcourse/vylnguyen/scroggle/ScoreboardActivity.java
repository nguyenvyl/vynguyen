package edu.neu.madcourse.vylnguyen.scroggle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import edu.neu.madcourse.vylnguyen.R;
import edu.neu.madcourse.vylnguyen.Singleton;
import edu.neu.madcourse.vylnguyen.scroggle.models.Score;
import edu.neu.madcourse.vylnguyen.scroggle.models.User;

import static edu.neu.madcourse.vylnguyen.R.raw.j;
import static edu.neu.madcourse.vylnguyen.R.raw.r;

public class ScoreboardActivity extends AppCompatActivity {

    private static final String TAG = "ScoreboardActivity";

    static private int rankIds[] = {R.id.rank_1, R.id.rank_2, R.id.rank_3,
            R.id.rank_4, R.id.rank_5, R.id.rank_6, R.id.rank_7, R.id.rank_8,
            R.id.rank_9, R.id.rank_10};
    static private int dateIds[] = {R.id.date_1, R.id.date_2, R.id.date_3,
            R.id.date_4, R.id.date_5, R.id.date_6, R.id.date_7, R.id.date_8,
            R.id.date_9, R.id.date_10};
    static private int scoreIds[] = {R.id.score_1, R.id.score_2, R.id.score_3,
            R.id.score_4, R.id.score_5, R.id.score_6, R.id.score_7, R.id.score_8,
            R.id.score_9, R.id.score_10};
    static private int bestWordIds[] = {R.id.best_word_1, R.id.best_word_2, R.id.best_word_3,
            R.id.best_word_4, R.id.best_word_5, R.id.best_word_6, R.id.best_word_7, R.id.best_word_8,
            R.id.best_word_9, R.id.best_word_10};
    static private int bestWordScoreIds[] = {R.id.best_word_score_1, R.id.best_word_score_2, R.id.best_word_score_3,
            R.id.best_word_score_4, R.id.best_word_score_5, R.id.best_word_score_6, R.id.best_word_score_7, R.id.best_word_score_8,
            R.id.best_word_score_9, R.id.best_word_score_10};

//    String username = "poopoopoint";
    private DatabaseReference mDatabase;
    ArrayList<Score> scores = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "on create");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scroggle_activity_scoreboard);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d(TAG, "onDataChange");
                        Log.d(TAG, scores.toString());
//                        sortBestScores();
//                        populateScores();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );

        mDatabase.child("Users").child(Singleton.getInstance().getUserKey()).addChildEventListener(
                new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Log.d(TAG, dataSnapshot.getChildren().toString());
                        for(DataSnapshot score : dataSnapshot.getChildren()){
                            Log.d(TAG, score.getValue(Score.class).toString());
                            scores.add(score.getValue(Score.class));
                        }
                        sortBestScores();
                        populateScores();
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        Log.e(TAG, "onChildRemoved: dataSnapshot = " + dataSnapshot.getValue());

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                        Log.e(TAG, "onChildMoved: dataSnapshot = " + dataSnapshot.getValue());

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, "onCancelled:" + databaseError);
                    }
                }
        );
    }


    private void populateScores(){
        int size = 10;
        if(scores.size() < 10){
            size = scores.size();
        }

        for(int i = 0; i < size; i++){
            TextView rankView = (TextView) findViewById(rankIds[i]);
            TextView dateView = (TextView) findViewById(dateIds[i]);
            TextView scoreView = (TextView) findViewById(scoreIds[i]);
            TextView bestWordView = (TextView) findViewById(bestWordIds[i]);
            TextView bestWordScoreView = (TextView) findViewById(bestWordScoreIds[i]);

            Score score = scores.get(i);
            rankView.setText(Integer.toString(i + 1));
            dateView.setText(score.getDate());
            scoreView.setText(score.getScore().toString());
            bestWordView.setText(score.getBestWord());
            bestWordScoreView.setText(score.getWordScore().toString());
        }
    }


    private void sortBestScores(){
        Collections.sort(this.scores, new Comparator<Score>() {
            @Override
            public int compare(Score score1, Score score2) {
                return score2.getScore() - score1.getScore();
            }
        });
    }


}
