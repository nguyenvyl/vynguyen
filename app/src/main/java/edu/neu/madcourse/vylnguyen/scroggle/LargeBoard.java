package edu.neu.madcourse.vylnguyen.scroggle;

import android.app.Activity;
import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.Random;

import edu.neu.madcourse.vylnguyen.R;
import edu.neu.madcourse.vylnguyen.Singleton;

import static edu.neu.madcourse.vylnguyen.R.raw.i;
import static edu.neu.madcourse.vylnguyen.R.raw.t;
import static edu.neu.madcourse.vylnguyen.R.raw.thi;

public class LargeBoard {
    private static final String TAG = "LargeBoard";
    ArrayList<SmallBoard> boards = new ArrayList<SmallBoard>();
    Integer score = new Integer(0);
//    ArrayList<String> words = new ArrayList<String>();
    static private int smallBoardIds[] = {R.id.scrog_large1, R.id.scrog_large2, R.id.scrog_large3,
            R.id.scrog_large4, R.id.scrog_large5, R.id.scrog_large6, R.id.scrog_large7, R.id.scrog_large8,
            R.id.scrog_large9,};
    private View largeBoardView;
    private StringBuilder currentString;
//    public Integer lastMove;
    public String bestWord;
    public Integer bestWordScore;


    public GameFragment getParent() {
        return parent;
    }

    public void setParent(GameFragment parent) {
        this.parent = parent;
    }

    private GameFragment parent;


    public View getLargeBoardView() {
        return largeBoardView;
    }

    public void setLargeBoardView(View largeBoardView) {
        this.largeBoardView = largeBoardView;
    }

    public LargeBoard(GameFragment parent, View rootView) {
        this.currentString = new StringBuilder();
        this.parent = parent;
        this.bestWord = new String("");
        this.bestWordScore = new Integer(0);
        initBoard(rootView);
    }

    public void initSmallBoardViews(View rootView){
        for(int i = 0; i < 9; i++){
            View smallBoardView = rootView.findViewById(smallBoardIds[i]);
            this.boards.get(i).setSmallBoardView(smallBoardView);
//            Log.d(TAG, "Small board view number " + Integer.toString(i) + " is set!");
            this.boards.get(i).initTileViews();
        }
    }

    public void initBoard(View rootView){
//        Log.d(TAG, "initBoard");
        ArrayList<String> randomWords = new ArrayList<String>();
        Integer numWords = Singleton.getInstance().get9letters().size();
        while(randomWords.size() < 9){
            Random rand = new Random();
            Integer randomIndex = rand.nextInt(numWords);
            String randomWord = Singleton.getInstance().get9letters().get(randomIndex);
            if(!randomWords.contains(randomWord)){
                randomWords.add(randomWord);
            }
        }
        for(int i = 0; i < 9; i++){
            String word = randomWords.get(i);
            this.boards.add(i, new SmallBoard(word, this, i));
        }
    }

    public void updateBestWord(String word, Integer wordScore){
        Log.d(TAG, "update best word");
        Log.d(TAG, "checking word " + word + " that has score " + wordScore.toString());
        Log.d(TAG, "Current best word: " + this.bestWord + " score: " + this.bestWordScore.toString());
        if(this.bestWordScore < wordScore){
            this.bestWord = word;
            this.bestWordScore = wordScore;
        }
    }

    /*************************************************************************************
     * Code for Phase 1
     **************************************************************************************/

    // Handling when a small board is clicked on
    public void onClick(Integer boardNumber){
//        Log.d(TAG, "Clicked on board number " + boardNumber.toString());
        for(int i = 0; i < 9; i++){
            if(i == boardNumber){
                this.boards.get(i).setActive(true);
            }
            else {
                this.boards.get(i).disableBoard();
            }
        }
    }

    public void restartGame(View rootView){
        this.score = 0;
        this.clearCurrentString();
        this.bestWord = "";
        this.bestWordScore = 0;
        for(int i = 0; i < 9; i++){
            View smallBoardView = rootView.findViewById(smallBoardIds[i]);
            this.boards.get(i).restartBoard(smallBoardView);
        }
    }

    public void onClear(){
//        Log.d(TAG, "onClear");
        for(int i = 0; i < 9; i++){
            this.boards.get(i).enableBoard();
        }
    }

    // Gets the active small board. Returns null if no boards are active.
    public SmallBoard getActiveBoard() {
        SmallBoard activeBoard = null;
        for(SmallBoard board : this.boards){
            if(board.getActive().equals(true)){
                activeBoard = board;
            }
        }
        return activeBoard;
    }


    public Integer onSubmit(){
        SmallBoard activeBoard = getActiveBoard();
//        Integer wordScore = activeBoard.scoreWord();

        // If no boards are active, do nothing.
        if(activeBoard == null || activeBoard.getCurrentString().length() < 3){
            return this.score;
        }
        else{
            Integer wordScore = activeBoard.submitBoard();
            this.score += wordScore;
            onClear();
            return this.score;
        }
    }

    /*************************************************************************************
     * Code for Phase 2
     **************************************************************************************/

    public void startPhase2(){
        for(int i = 0; i < 9; i++){
            this.boards.get(i).startPhase2();
        }
    }

    public void onPhase2Click(Tile currentTile){
//        Log.d(TAG, "onPhase2Click");
//        Log.d(TAG, "Current string is: " + this.currentString.toString());
        this.currentString.append(currentTile.getLetter());
        if(currentString.length() >= 3){
            loadDict(currentString);
        }
        Integer smallBoardPos = currentTile.parent.position;
        Graph graph = new Graph();
        ArrayList<Integer> neighbors = graph.getNeighbors(smallBoardPos);
    }

    public Integer onSubmitPhase2(){
        if(this.currentString.length() >= 3){
            String word = this.currentString.toString();
            Boolean isValid = Singleton.getInstance().getDictionaryClass().search(word);
            Integer wordScore = scoreWord(this.currentString.toString());
            final Activity currentActivity = this.parent.getActivity();
            final MediaPlayer validWordSound = new MediaPlayer().create(currentActivity, R.raw.valid_word);
            final MediaPlayer invalidWordSound = new MediaPlayer().create(currentActivity, R.raw.invalid_word);
            if(isValid){
                this.score += wordScore;
                updateBestWord(word, wordScore);
                validWordSound.start();
                for(int i = 0; i < 9; i++){
                    this.boards.get(i).phase2SubmitTiles();
                }
            } else {
                invalidWordSound.start();
                this.score -= wordScore;
            }
        }
        onClearPhase2();
        return this.score;
    }

    public void onClearPhase2(){
        this.clearCurrentString();
        for(int i = 0; i < 9; i++){
            this.boards.get(i).enableBoardPhase2();
        }
    }

    /*
     * Code for handling word lookup and scoring
     */

    public void clearCurrentString(){
        this.currentString.setLength(0);
        this.currentString.trimToSize();
    }

    public static void loadDict(StringBuilder word){
        Singleton.getInstance().getDictionaryClass().loadDict(word.toString());
    }

    public static Integer scoreWord(String word){
        Integer score = new Integer(0);
        for(int i = 0; i < word.length(); i++){
            score += getLetterValue(word.charAt(i));
        }
        return score;
    }

    public static Integer getLetterValue(Character letter){
        Integer value = 0;
        switch(letter){
            case 'a':
                value = 1;
                break;
            case 'b':
                value = 3;
                break;
            case 'c':
                value = 3;
                break;
            case 'd':
                value = 2;
                break;
            case 'e':
                value = 1;
                break;
            case 'f':
                value = 4;
                break;
            case 'g':
                value = 2;
                break;
            case 'h':
                value = 4;
                break;
            case 'i':
                value = 1;
                break;
            case 'j':
                value = 8;
                break;
            case 'k':
                value = 5;
                break;
            case 'l':
                value = 1;
                break;
            case 'm':
                value = 3;
                break;
            case 'n':
                value = 1;
                break;
            case 'o':
                value = 1;
                break;
            case 'p':
                value = 3;
                break;
            case 'q':
                value = 10;
                break;
            case 'r':
                value = 1;
                break;
            case 's':
                value = 1;
                break;
            case 't':
                value = 1;
                break;
            case 'u':
                value = 1;
                break;
            case 'v':
                value = 4;
                break;
            case 'w':
                value = 4;
                break;
            case 'x':
                value = 8;
                break;
            case 'y':
                value = 4;
                break;
            case 'z':
                value = 10;
                break;
        }
        return value;
    }



}
