package edu.neu.madcourse.vylnguyen.scroggle;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

import static edu.neu.madcourse.vylnguyen.R.raw.t;


/**
 * Created by nguyenvyl on 2/27/17.
 */

public class Graph {

    ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
    static private final String TAG = "Graph";
    static private final ArrayList<Integer> ALL = new ArrayList<>(Arrays.asList(0,1,2,3,4,5,6,7,8));

    // This is not a very beautiful constructor I'm sorry
    // This graph is always the same which is why I did it like this
    public Graph(){
        ArrayList<Integer> zero = new ArrayList<>();
        ArrayList<Integer> one = new ArrayList<>();
        ArrayList<Integer> two = new ArrayList<>();
        ArrayList<Integer> three = new ArrayList<>();
        ArrayList<Integer> four = new ArrayList<>();
        ArrayList<Integer> five = new ArrayList<>();
        ArrayList<Integer> six = new ArrayList<>();
        ArrayList<Integer> seven = new ArrayList<>();
        ArrayList<Integer> eight = new ArrayList<>();

        zero.add(1);
        zero.add(3);
        zero.add(4);
        this.graph.add(0, zero);

        one.add(0);
        one.add(2);
        one.add(3);
        one.add(4);
        one.add(5);
        this.graph.add(1, one);

        two.add(1);
        two.add(4);
        two.add(5);
        this.graph.add(2, two);

        three.add(0);
        three.add(1);
        three.add(4);
        three.add(6);
        three.add(7);
        this.graph.add(3, three);

        four.add(0);
        four.add(1);
        four.add(2);
        four.add(3);
        four.add(5);
        four.add(6);
        four.add(7);
        four.add(8);
        this.graph.add(4, four);

        five.add(1);
        five.add(2);
        five.add(4);
        five.add(7);
        five.add(8);
        this.graph.add(5, five);

        six.add(3);
        six.add(4);
        six.add(7);
        this.graph.add(6, six);

        seven.add(3);
        seven.add(4);
        seven.add(5);
        seven.add(6);
        seven.add(8);
        this.graph.add(7, seven);

        eight.add(4);
        eight.add(5);
        eight.add(7);
        this.graph.add(8, eight);
    }

    public ArrayList<ArrayList<Integer>> getGraph() {
        return graph;
    }

    public ArrayList<Integer> getNeighbors(Integer i){
        if(i.equals(-1)){
            return ALL;
        }
        return this.graph.get(i);
    }

    // Generates a layout based on a random BFS graph traversal
    public ArrayList<Integer> generateLayout(){
//        Log.d(TAG, "generateLayout");

        ArrayList<Integer> layout = new ArrayList<>();
        HashSet<Integer> visited = new HashSet<>();

        Queue<Integer> toVisit = new LinkedList<>();
        Random rand = new Random();
        Integer randomIndex = rand.nextInt(9);

        toVisit.add(randomIndex);
        layout.add(randomIndex);

        while(!toVisit.isEmpty()){
            Integer currentTile = toVisit.poll();
            visited.add(currentTile);
            List<Integer> current = this.graph.get(currentTile);
            ArrayList<Integer> unvisitedNeighbors = new ArrayList<>();
            for(Integer neighbor : current){
                if(!visited.contains(neighbor)){
                    unvisitedNeighbors.add(neighbor);
                }
            }

            if(unvisitedNeighbors.size() == 0){
//                Log.d(TAG, "Generated layout " + layout.toString());
                if(layout.size() != 9){
                    return generateLayout();
                }
                else{
                    return layout;
                }
            }
            else{
                Integer randomNeighbor = rand.nextInt(unvisitedNeighbors.size());
                Integer nextTile = unvisitedNeighbors.get(randomNeighbor);
                layout.add(nextTile);
                toVisit.add(nextTile);
            }
        }
        if( layout.size() != 9 ){
            return generateLayout();
        }
        return layout;
    }

}
