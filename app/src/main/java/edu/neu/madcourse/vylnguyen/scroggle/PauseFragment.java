/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband4 for more book information.
 ***/
package edu.neu.madcourse.vylnguyen.scroggle;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.neu.madcourse.vylnguyen.R;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

public class PauseFragment extends Fragment {

    private static final String TAG = "PauseFragment";

    // Fragment for the "restart game" and "main menu" buttons on the game screen
    // Main button finishes the activity.
    // Restart button starts a new game activity from scratch.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "PauseFragment on create view");
//        Log.d(TAG, "Inflater:");
//        Log.d(TAG, inflater.toString());

        View rootView =
                inflater.inflate(R.layout.scroggle_pause_control, container, false);
//        Log.d(TAG, "Root view: ");
//        Log.d(TAG, rootView.toString());
        View resume = rootView.findViewById(R.id.scrog_resume_button);
        View pauseText = rootView.findViewById(R.id.pause_text);

        final GameActivity gameActivity = (GameActivity) getActivity();

        resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gameActivity.setContentView(R.layout.scroggle_activity_game);
                ((edu.neu.madcourse.vylnguyen.scroggle.GameActivity) getActivity()).resumeGame();
            }
        });
        rootView.setVisibility(View.GONE);
        resume.setVisibility(View.GONE);
        pauseText.setVisibility(View.GONE);
        return rootView;
    }

}
