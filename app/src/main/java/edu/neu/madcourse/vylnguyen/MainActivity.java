package edu.neu.madcourse.vylnguyen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
    }

    public void showAbout(View view){
        Intent intent = new Intent(this, DisplayAboutActivity.class);
        startActivity(intent);
        this.finish();
    }

    public void generateError(View view){
        throw new RuntimeException("This ship is sinking, kids!");
    }

    public void showDictionary(View view){
        Intent intent = new Intent(this, TestDictionaryActivity.class);
        startActivity(intent);
        this.finish();
    }

    public void launchTicTacToe(View view) {
        Intent intent = new Intent(this, edu.neu.madcourse.vylnguyen.tictactoe.MainActivity.class);
        startActivity(intent);
        this.finish();
    }

    public void launchScroggle(View view) {
        Log.i("Main activity screen", "Launch Scroggle method");
        Intent intent = new Intent(this, edu.neu.madcourse.vylnguyen.scroggle.MainActivity.class);
        startActivity(intent);
        this.finish();
    }

}
