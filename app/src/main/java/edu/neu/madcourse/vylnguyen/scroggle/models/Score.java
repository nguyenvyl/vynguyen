package edu.neu.madcourse.vylnguyen.scroggle.models;

import java.text.SimpleDateFormat;

/**
 * Created by nguyenvyl on 3/11/17.
 */

public class Score {
    Integer score;
    String bestWord;
    Integer wordScore;
    String date;

    public Score(){

    }

    @Override
    public String toString() {
        return "Score{" +
                "score=" + score +
                ", bestWord='" + bestWord + '\'' +
                ", wordScore=" + wordScore +
                ", date='" + date + '\'' +
                '}';
    }

    public Score(Integer score, String bestWord, Integer wordScore, String date) {
        this.score = score;
        this.bestWord = bestWord;
        this.wordScore = wordScore;
        this.date = date;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getBestWord() {
        return bestWord;
    }

    public void setBestWord(String bestWord) {
        this.bestWord = bestWord;
    }

    public Integer getWordScore() {
        return wordScore;
    }

    public void setWordScore(Integer wordScore) {
        this.wordScore = wordScore;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
