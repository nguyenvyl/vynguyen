package edu.neu.madcourse.vylnguyen;

import android.app.Application;
import android.content.Context;

/**
 * Created by nguyenvyl on 2/28/17.
 */

public class MyApplication extends Application {

    private static Context mContext;

    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }

    public static Context getAppContext() {
        return mContext;
    }

}
