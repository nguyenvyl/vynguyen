/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband4 for more book information.
 ***/
package edu.neu.madcourse.vylnguyen.scroggle;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.neu.madcourse.vylnguyen.R;

public class ControlFragment extends Fragment {

    private static final String TAG = "ControlFragment";

    // Fragment for the "restart game" and "main menu" buttons on the game screen
    // Main button finishes the activity.
    // Restart button starts a new game activity from scratch.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "ControlFragment onCreateView");
        Log.d(TAG, "Inflater:");
        Log.d(TAG, inflater.toString());

        View rootView =
                inflater.inflate(R.layout.scroggle_fragment_control, container, false);
        View main = rootView.findViewById(R.id.scrog_button_main);
        View restart = rootView.findViewById(R.id.scrog_button_restart);
        View pause = rootView.findViewById(R.id.scrog_button_pause);

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((edu.neu.madcourse.vylnguyen.scroggle.GameActivity) getActivity()).restartGame();
            }
        });
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((edu.neu.madcourse.vylnguyen.scroggle.GameActivity) getActivity()).pauseGame();
            }
        });
        return rootView;
    }

}
