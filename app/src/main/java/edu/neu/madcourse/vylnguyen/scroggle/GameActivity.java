/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband4 for more book information.
 ***/
package edu.neu.madcourse.vylnguyen.scroggle;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import edu.neu.madcourse.vylnguyen.R;
import edu.neu.madcourse.vylnguyen.Singleton;
import edu.neu.madcourse.vylnguyen.scroggle.models.User;

import static android.content.ContentValues.TAG;
import static edu.neu.madcourse.vylnguyen.R.raw.c;
import static edu.neu.madcourse.vylnguyen.R.raw.his;
import static edu.neu.madcourse.vylnguyen.R.string.score;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

public class GameActivity extends Activity {

    // Save user preferences and any saved games

    private MediaPlayer mMediaPlayer;
    private Handler mHandler = new Handler();
    private edu.neu.madcourse.vylnguyen.scroggle.GameFragment mGameFragment;
    private edu.neu.madcourse.vylnguyen.scroggle.PauseFragment mPauseFragment;
    private static final String TAG = "GameActivity";
    private static final String USER_KEY = "user_key";
    private static final String USERNAME = "username";
    private static final String TOKEN = "token";
    private static final String CLIENT_REGISTRATION_TOKEN = "eDcMLnWOHVc:APA91bH35a2pOcSxSEWv7dgHZtbxXP6v_XHv1HTY31IXTPr5BNgD1Xb5cJy_EN5BonX58-4FcHKI_yCedBEwEzaFXaxH61eCJXhu7FMxUEyIPaiZTDSvYXHPchHyK2I18MedxNy4-Bkq";
    private static final String SERVER_KEY = "key=AAAALnc5Jwk:APA91bFC4Hd86H8TcVwdw7rJnbEbkoMiasg2GKUWq7s6Vz1n05ekdLHy9cdJbX0nqfZYOKWgGh5bST7FJYi5hvriaBAR0IMnXtfEv7gpuOatXgU1yvTqNusGGSHGz4t3pkhMRMZImZ0d";
    private static final Integer PHASE_TIME = 10000;


    @Override
    // Sets layout to activity_game
    // Restores previous game if restore key exists
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scroggle_activity_game);
        Singleton.getInstance().setTimeRemaining(PHASE_TIME);
        Log.d(TAG, "Game activity on create");


        mGameFragment = (edu.neu.madcourse.vylnguyen.scroggle.GameFragment) getFragmentManager()
                .findFragmentById(R.id.scroggle_fragment_game);

        final GameFragment gameFragment = this.mGameFragment;
        final GameActivity gameActivity = this;

        setSavedUser();

        View resume = findViewById(R.id.scrog_resume_button);
        resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resumeGame();
//                gameFragment.resumeGame(gameActivity);
            }
        });
    }

    private String getUserKey(){
        return PreferenceManager.getDefaultSharedPreferences(this.getApplication()).getString(USER_KEY, "");
    }

    private String getUsername(){
        return PreferenceManager.getDefaultSharedPreferences(this.getApplication()).getString(USERNAME, "");
    }

    public void setSavedUser(){
        Log.d(TAG, "set saved user");
        String userKey = getUserKey();
        String username = getUsername();
        mGameFragment.setUserKey(userKey);
        mGameFragment.setUsername(username);
        Log.d(TAG, "retrieved user " + mGameFragment.getUserKey() + ", " + mGameFragment.getUsername());
    }

    // Starts the music when the app is resumed
    @Override
    protected void onResume() {
        super.onResume();
        mMediaPlayer = MediaPlayer.create(this, R.raw.ynwainstrumental);
        mMediaPlayer.setLooping(true);
        mMediaPlayer.start();
    }

    // When app is paused:
    // - Stops music
    // - Saves preferences
    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeCallbacks(null);
        mMediaPlayer.stop();
        mMediaPlayer.reset();
        mMediaPlayer.release();
    }

    public void restartGame() {
        this.mGameFragment.restartGame();
    }

    public void pauseGame() {
        Log.d(TAG, "pause game - Game activity");
        this.mGameFragment.paused = true;
        this.mGameFragment.timer.cancel();
        View pause = findViewById(R.id.pause_screen);
        pause.setVisibility(View.VISIBLE);
    }

    public void resumeGame(){
        Log.d(TAG, "resume game - Game activity");
        Log.d(TAG, "time remaining = " + Long.toString(Singleton.getInstance().getTimeRemaining()));
        this.mGameFragment.paused = false;
        this.mGameFragment.initTimer(Singleton.getInstance().getTimeRemaining());
        this.mGameFragment.timer.start();
        View pause = findViewById(R.id.pause_screen);
        pause.setVisibility(View.GONE);
    }


}

