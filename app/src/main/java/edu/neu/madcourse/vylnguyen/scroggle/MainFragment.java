/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband4 for more book information.
 ***/
package edu.neu.madcourse.vylnguyen.scroggle;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.neu.madcourse.vylnguyen.R;

public class MainFragment extends Fragment {

    private AlertDialog mDialog;

    // Creates the home screen for Scroggle.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =
                inflater.inflate(R.layout.scroggle_main_fragment, container, false);
        // Handle buttons here...
        View newButton = rootView.findViewById(R.id.scrog_new_button);
//      View continueButton = rootView.findViewById(R.id.scrog_continue_button);
        View aboutButton = rootView.findViewById(R.id.scrog_about_button);
        View scoreboardButton = rootView.findViewById(R.id.scrog_scoreboard_button);
        View leaderboardButton = rootView.findViewById(R.id.scrog_leaderboard_button);
        View changeUsernameButton = rootView.findViewById(R.id.scrog_change_username);
        View messageUserButton = rootView.findViewById(R.id.scrog_message_button);

        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), edu.neu.madcourse.vylnguyen.scroggle.GameActivity.class);
                getActivity().startActivity(intent);
            }
        });

        scoreboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Clicked scoreboard", "starting activity...");
                Intent intent = new Intent(getActivity(), edu.neu.madcourse.vylnguyen.scroggle.ScoreboardActivity.class);
                getActivity().startActivity(intent);
            }
        });

        leaderboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Clicked leaderboard", "starting activity...");
                Intent intent = new Intent(getActivity(), LeaderboardActivity.class);
                getActivity().startActivity(intent);
            }
        });

        changeUsernameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Clicked change username", "starting activity...");
                Intent intent = new Intent(getActivity(), ChangeUsernameActivity.class);
                getActivity().startActivity(intent);
            }
        });

        messageUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Clicked message user", "starting activity...");
                Intent intent = new Intent(getActivity(), MessageUserActivity.class);
                getActivity().startActivity(intent);
            }
        });

        // Sets onClick for "about" button
        // Shows an alert dialogue with the "About" text
        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.scroggle_about_text);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // nothing
                            }
                        });
                mDialog = builder.show();
            }
        });
        return rootView;
    }

    // Handles when app is paused
    @Override
    public void onPause() {
        super.onPause();

        // Get rid of the about dialog if it's still up
        if (mDialog != null)
            mDialog.dismiss();
    }
}
